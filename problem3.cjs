// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.//

export function problem3 (inventory){
    let alphab_sorted_car = inventory.map((elem) => {
        return elem;
    })
        .sort((a, b) => {
            let model_a_name = a.car_model.toUpperCase();
            let model_b_name = b.car_model.toUpperCase();

            if (model_a_name < model_b_name) {
                return -1;
            }
            if (model_a_name > model_b_name) {
                return 1;
            }

            return 0;
        })
    return alphab_sorted_car;
}