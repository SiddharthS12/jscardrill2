// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
// console.log(__dirname);
function problem1(inventory= [], findId) {
  const empty = []
  if (typeof inventory === "undefined" || !Array.isArray(inventory)) {
    return empty
  }
  if (inventory.length === 0) {
    return empty
  }
  if (typeof findId !== "number") {
    return empty;
  }
  let car = inventory.filter((element) => {
    return element.id === findId;
  });
  return car[0];
}

module.exports = problem1