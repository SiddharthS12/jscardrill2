// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
//

export function problem6(inventory) {
  let BMW_AUDI_CARS = inventory.filter((elem) => {
    return elem.car_make == "BMW" || elem.car_make == "Audi";
  });
  return BMW_AUDI_CARS;
}
