const inventory = require("../Inventory.cjs");
const problem1 = require("../problem1.cjs");
const findId = 33;
const result_obj = problem1(inventory, findId);
// console.log(result_obj);

if (Array.isArray(result_obj) || result_obj === "undefined") {
  console.log([]);
} else {
  console.log(
    `Car 33 is a ${result_obj.car_year} ${result_obj.car_make} ${result_obj.car_model}`
  );
}
