import inventory from "../Inventory.js";
import { problem4 } from "../problem/problem4.js";

const result_obj = problem4(inventory);
// console.log(result_obj);

console.log(`Car years are:- `);
for (let i = 0; i < result_obj.length; i++) {
  let car_year = result_obj[i];
  console.log(car_year);
}
