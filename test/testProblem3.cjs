import inventory from "../Inventory.js";
import { problem3 } from "../problem/problem3.js";

const result_obj = problem3(inventory);
// console.log(result_obj);

console.log(`Car models in alphabetical order are:- `);
for (let i = 0; i < result_obj.length; i++){
    let car_model = result_obj[i]["car_model"]
    console.log(car_model);
}