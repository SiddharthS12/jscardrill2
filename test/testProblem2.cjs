import inventory from "../Inventory.js";
import { problem2 } from "../problem/problem2.js";

const result_obj = problem2(inventory);
// console.log(result_obj);

console.log(`Last Car is a ${result_obj.car_make} ${result_obj.car_model}`);