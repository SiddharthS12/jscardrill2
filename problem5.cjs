// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
//

export function problem5(inventory) {
  let cars_before_2000 = inventory.filter((elem) => {
    return elem.car_year < 2000
  })
  return cars_before_2000;
}
